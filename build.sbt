import java.nio.file.{Files, StandardCopyOption}
import java.util.UUID

import scala.sys.process.Process

val gitPath = "/usr/bin/git"
val swigPath = "/usr/bin/swig"
val gccPath = "/usr/bin/gcc"

lazy val generate = taskKey[Seq[File]]("Generate sources")
lazy val link = taskKey[Seq[File]]("Generate libraries")

lazy val root = (project in file(".")).settings(
  name := "java-libwebp-bindgen",
  description := "Custom libwebp binding generation that includes animation support",
  organization := "tf.bug",
  version := "0.1.0",
  crossPaths := false,
  autoScalaLibrary := false,
  publishMavenStyle := true,
  resolvers += "jitpack" at "https://jitpack.io",
  libraryDependencies ++= Seq(
    "com.github.adamheinrich" % "native-utils" % "e6a3948966",
  ),
  generate := {
    buildBindings((sourceManaged in Compile).value)
  },
  link := {
    buildLibrary((resourceManaged in Compile).value)
  },
  sourceGenerators in Compile += generate,
  resourceGenerators in Compile += link,
)

def copy(file: File, root: File): Unit = {
  if(!root.exists()) root.mkdirs()
  if(!root.isDirectory) throw new IllegalArgumentException("Root not a directory")
  if(file.isDirectory) {
    file.listFiles().foreach(f => copy(f, new File(root, file.getName)))
  } else {
    val nf = new File(root, file.getName)
    Files.copy(file.toPath, nf.toPath, StandardCopyOption.REPLACE_EXISTING)
  }
}

def buildBindings(base: File): Seq[File] = {
  val swig =
    """
      |%module libwebp
      |
      |%include "enums.swg"
      |
      |%{
      |#include "../webp/encode.h"
      |#include "../webp/decode.h"
      |#include "../webp/mux.h"
      |#include "../webp/demux.h"
      |%}
      |
      |%include "../webp/types.h"
      |%include "../webp/mux_types.h"
      |%include "../webp/encode.h"
      |%include "../webp/decode.h"
      |%include "../webp/mux.h"
      |%include "../webp/demux.h"
    """.stripMargin
  val rb: File = root.base
  Process(gitPath :: "submodule" :: "update" :: Nil, rb).!
  val lwp: File = rb / "libwebp"
  val lwps: File = lwp / "src"
  val td: File = Files.createTempDirectory(s"${UUID.randomUUID().toString}").toFile
  copy(lwps, td)
  val news: File = td / "src"
  val bg: File = news / "bindgen"
  bg.mkdirs()
  val i: File = bg / "libwebp.i"
  Files.write(i.toPath, swig.getBytes("UTF-8"))
  val od = bg / "java" / "com" / "google" / "webp"
  od.mkdirs()
  Process(swigPath :: "-java" :: "-package" :: "com.google.webp" :: "-outdir" :: od.getAbsolutePath :: "-o" :: (bg / "libwebp_java_wrap.c").getAbsolutePath :: i.getAbsolutePath :: Nil, bg).!
  println(bg.getAbsolutePath)
  copy(bg / "java" / "com", base)
  def childFiles(f: File): Seq[File] = {
    if(f.isDirectory) {
      f.listFiles().toSeq.flatMap(childFiles)
    } else {
      Seq(f)
    }
  }
  childFiles(base / "com")
}

def buildLibrary(base: File): Seq[File] = {
  val swig =
    """
      |%module libwebp
      |
      |%include "enums.swg"
      |
      |%{
      |#include "../webp/encode.h"
      |#include "../webp/decode.h"
      |#include "../webp/mux.h"
      |#include "../webp/demux.h"
      |%}
      |
      |%include "../webp/types.h"
      |%include "../webp/mux_types.h"
      |%include "../webp/encode.h"
      |%include "../webp/decode.h"
      |%include "../webp/mux.h"
      |%include "../webp/demux.h"
    """.stripMargin
  val rb: File = root.base
  Process(gitPath :: "submodule" :: "update" :: Nil, rb).!
  val lwp: File = rb / "libwebp"
  val lwps: File = lwp / "src"
  val td: File = Files.createTempDirectory(s"${UUID.randomUUID().toString}").toFile
  copy(lwps, td)
  val news: File = td / "src"
  val bg: File = news / "bindgen"
  bg.mkdirs()
  val i: File = bg / "libwebp.i"
  Files.write(i.toPath, swig.getBytes("UTF-8"))
  val od = bg / "java" / "com" / "google" / "webp"
  od.mkdirs()
  Process(swigPath :: "-java" :: "-package" :: "com.google.webp" :: "-outdir" :: od.getAbsolutePath :: "-o" :: (bg / "libwebp_java_wrap.c").getAbsolutePath :: i.getAbsolutePath :: Nil, bg).!
  println(bg.getAbsolutePath)
  val lo = bg / "libwebp_jni.so"
  Process(gccPath :: "-shared" :: "-fPIC" :: "-fno-strict-aliasing" :: "-O2" :: "-I/usr/lib/jvm/java-8-jdk/include" :: "-I/usr/lib/jvm/java-8-jdk/include/linux" :: (bg / "libwebp_java_wrap.c").getAbsolutePath :: "-lwebp" :: "-o" :: lo.getAbsolutePath :: Nil, bg).!
  copy(lo, base)
  Seq(base / lo.getName)
}
