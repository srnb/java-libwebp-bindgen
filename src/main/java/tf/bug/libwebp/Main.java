package tf.bug.libwebp;

import com.google.webp.SWIGTYPE_p_WebPMux;
import com.google.webp.WebPData;
import com.google.webp.WebPMuxFrameInfo;
import com.google.webp.libwebp;
import cz.adamh.utils.NativeUtils;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        NativeUtils.loadLibraryFromJar("/libwebp_jni.so");
        System.out.println(libwebp.WebPGetMuxVersion());
    }

}
